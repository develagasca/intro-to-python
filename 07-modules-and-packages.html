<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <title>Modules and Packages</title>

        <link rel="stylesheet" href="css/reveal.css">
        <link rel="stylesheet" href="css/theme/solarized.css">

        <!-- Theme used for syntax highlighting of code -->
        <link rel="stylesheet" href="lib/css/zenburn.css">

        <!-- Printing and PDF exports -->
        <script>
            var link = document.createElement( 'link' );
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
            document.getElementsByTagName( 'head' )[0].appendChild( link );
        </script>
    </head>
    <body>
        <div class="reveal">
            <div class="slides">

                <section><h1>Modules</h1></section>

                <section>
                    <h2>Modules</h2>

                    <section>
                        <p>A module is a file containing Python code &mdash; definitions and statements &mdash; with the suffix of <code>.py</code>.</p>
                        <p>Examples: <code>hello.py</code>, <code>greeter.py</code>, <code>awesome_python.py</code></p>
                        <p>The file name is the module name e.g. <code>hello.py</code> has the module name <code>hello</code>.</p>
                        <p>And, yes, there are lots of built-in modules that come with Python</p>
                    </section>

                    <section>
                        <h3>Recommended Naming of Modules</h3>
                        <ul>
                            <li>Use of short, all lowercase names for modules is encouraged</li>
                            <li>Underscores may be used in the module name if it improves readability e.g. <code>hello_world.py</code></li>
                            <li>Avoid using the dash (&dash;) in module names e.g. <code>hello-world.py</code></li>
                            <li>DON'T use the dot (.) in module names e.g. <code>hello.world.py</code></li>
                        </ul>
                    </section>

                    <section>
                        <h3>Creating Modules</h3>
                        <p>Creating your very own Python module is very simple; simply create a new <code>.py</code> file with the desired module name.</p>
                    </section>
                </section>

                <section>
                    <h2><code>import</code> Statement</h2>

                    <section data-markdown>
                        <textarea data-template>
                            To use a module, we use the `import` statement.

                            ```python
                            &gt;&gt;&gt; import math
                            &gt;&gt;&gt; math.pi
                            3.141592653589793
                            ```

                            In this example, we imported the built-in `math` module. This gives us access to all objects declared in the `math` module such as the `pi` variable. Objects from the `math` module may be used using the <em>dot</em> (.) <em>notation</em>. Now you know why you shouldn't use dots in module names.
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            Modules may be imported into other modules. Assume we have 2 files, `greet.py` and `greetings.py`, in the same folder.

                            ```python
                            ### greetings.py
                            hello = 'Hello'
                            hi = 'Hi'
                            ```

                            ```python
                            ### greet.py
                            import greetings

                            def greeter(name, greeting):
                                print(greeting + ' ' + name)

                            greeter('Python', greetings.hello)  # prints out Hello Python
                            greeter('Python', greetings.hi)     # prints out Hi Python
                            ```
                        </textarea>
                    </section>
                </section>

                <section>
                    <h2><code>from</code> ... <code>import</code> Statement</h2>

                    <section data-markdown>
                        <textarea data-template>
                            If you just want to import specific objects from a module, the `from` _`<module>`_ `import` _`<object[s]>`_ statement may be used. From the prevous example, `greet.py` can be rewritten to this.

                            ```python
                            ### greet.py
                            from greetings import hello, hi

                            def greeter(name, greeting):
                                print(greeting + ' ' + name)

                            greeter('Python', hello)  # prints out Hello Python
                            greeter('Python', hi)     # prints out Hi Python
                            ```
                        </textarea>
                    </section>

                    <section>
                        <p>Take note that the variables <code>hello</code> and <code>hi</code> were called directly, and didn't need to be called like <code>greetings.hello</code> and <code>greetings.hi</code> in the <code>greet.py</code> module.</p>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            Also take note that the value of `hello` may be overriden by a locally declared variable in the `greet.py` module if it comes after the import.

                            ```python
                            ### greet.py
                            from greetings import hello, hi

                            hello = 'Hola'

                            def greeter(name, greeting):
                                print(greeting + ' ' + name)

                            greeter('Python', hello)  # prints out Hola Python
                            greeter('Python', hi)     # prints out Hi Python
                            ```
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ### `from` _`&lt;m&gt;`_ `import *`

                            This statement imports the module _`&lt;m&gt;`_ and creates references to all the public objects defined by that module. After you've run this statement, you can simply use the plain object name to refer to things defined in module _`&lt;m&gt;`_. But _`&lt;m&gt;`_ itself is not defined, so _`&lt;m&gt;.obj`_ doesn't work.
                        </textarea>
                    </section>
                </section>

                <section><h1>Packages</h1></section>

                <section>
                    <h2>Packages</h2>

                    <section data-markdown>
                        <textarea data-template>
                            Packages are namespaces which contain multiple packages and modules themselves. They are simply directories.

                            Each package in Python is a directory that __MUST__ contain a special file called `__init__.py`.

                            The name of the package is the name of the directory.
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            A sample directory structure of a Python package.

                            ```shell
                            greeter          # the directory, or the Python package
                            ├── __init__.py
                            ├── goodbye.py
                            ├── hello.py
                            └── hi.py
                            ```

                            The `__init__.py` file indicates that the directory is a Python package.

                            Python packages are imported the same way as a module is imported.
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            The `__init__.py` file may be empty; or can contain definitions and statements.

                            It executes when the module is loaded. It is analogous to the constructor for classes (more on classes later).
                        </textarea>
                    </section>
                </section>

                <section>
                    <h2>Importing a Package</h2>

                    <section data-markdown>
                        <textarea data-template>
                            As mentioned, like modules, packages can be imported using the `import` _`<p>`_, `from` _`<p>`_ `import x, y, z` or `from` _`<p>`_ `import *` statements.

                            ```python
                            &gt;&gt;&gt; import greeter
                            &gt;&gt;&gt; from greeter import goodbye, hello, hi
                            &gt;&gt;&gt; from greeter import *
                            ```
                        </textarea>
                    </section>
                </section>

                <section>
                    <h2>Nested Packages</h2>

                    <section data-markdown>
                        <textarea data-template>
                            ```shell
                            greeter
                            ├── __init__.py
                            ├── goodbye.py
                            ├── hello.py
                            └── lang
                                ├── __init__.py
                                ├── de
                                │   ├── __init__.py
                                │   ├── auf_wiedersehen.py
                                │   └── hallo.py
                                └── es
                                    ├── __init__.py
                                    ├── adios.py
                                    └── hola.py 
                            ```
                            ```python
                            &gt;&gt;&gt; from greeter.lang import de, es
                            &gt;&gt;&gt; from greeter.lang.de import hallo
                            ```
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ### `as` Keyword

                            The `as` keyword may be used to make an alias of the imported module in the namespace.

                            ```python
                            ### main.py
                            import greeter.lang.de.hallo

                            print(greeter.lang.de.hallo.hallo)  # print out the value of "hallo" variable
                            ```
                            ```python
                            ### main.py
                            import greeter.lang.de.hallo as de_hallo

                            print(de_hallo.hallo)  # print out the value of "hallo" variable
                            ```
                        </textarea>
                    </section>
                </section>

                <section>
                    <h2>Third-party Modules and Packages</h2>
                    <section>
                        <p>As mentioned earlier, Python is a huge community. It is also a very generous one as evident with how many community-contributed modules and packages are almost always free to download.</p>
                        <p>These modules and packages are located in a public repository known as the <strong>PyPI</strong> or the Python Package Index.</p>
                    </section>
                    <section>
                        <p>The <code>pip</code> tool, which comes with Python, is used to install packages from PyPI. It is usually run from what is called the command-line interface (CLI) of the operating system.</p>
                    </section>
                    <section data-markdown>
                        <textarea data-template>
                            ```shell
                            C:\Users\devel> pip --help

                            Usage:
                              pip &lt;command&gt; [options]

                            Commands:
                              install        Install packages.
                              download       Download packages.
                              uninstall      Uninstall packages.
                              freeze         Output installed packages in requirements format.
                              list           List installed packages.
                              ...
                              help           Show help for commands.
                            ```

                            `pip` from the Windows 10 `cmd.exe` CLI
                        </textarea>
                    </section>
                    <section>
                        <p>As an example, one of the most used package for web development using Python, the Django web framework, is just one huge Python package that can be installed from PyPI.</p>
                        <p>More at <a href="https://pypi.python.org">https://pypi.python.org</a></p>
                    </section>
                </section>

                <section>
                    <h2><a href="08-files.html">Next: Files</a></h2>
                </section>

            </div>
        </div>

        <script src="lib/js/head.min.js"></script>
        <script src="js/reveal.js"></script>

        <script>
            // More info about config & dependencies:
            // - https://github.com/hakimel/reveal.js#configuration
            // - https://github.com/hakimel/reveal.js#dependencies
            Reveal.initialize({
                dependencies: [
                    { src: 'plugin/markdown/marked.js' },
                    { src: 'plugin/markdown/markdown.js' },
                    { src: 'plugin/notes/notes.js', async: true },
                    { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
                ]
            });
        </script>
    </body>
</html>
