<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <title>Functions</title>

        <link rel="stylesheet" href="css/reveal.css">
        <link rel="stylesheet" href="css/theme/solarized.css">

        <!-- Theme used for syntax highlighting of code -->
        <link rel="stylesheet" href="lib/css/zenburn.css">

        <!-- Printing and PDF exports -->
        <script>
            var link = document.createElement( 'link' );
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
            document.getElementsByTagName( 'head' )[0].appendChild( link );
        </script>
    </head>
    <body>
        <div class="reveal">
            <div class="slides">

                <section><h1>Functions</h1></section>

                <section>
                    <h2>Functions</h2>
                    <section data-markdown>
                        <textarea data-template>
                            A function is a sequence of program instructions that perform a specific task, packaged as a unit. This unit can then be used in programs wherever that particular task should be performed.
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            The `def` keyword introduces a function _definition_. It must be followed by the function name and the parenthesized list of formal parameters. The statements that form the body of the function start at the next line, and must be indented.

                            The first statement of the function body can optionally be a string literal; this string literal is the function's documentation string, or _docstring_.
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ### Defining Functions

                            ```python
                            &gt;&gt;&gt; def shout():  # function with no parameters
                            ...     """Print a shout."""
                            ...     print('HELLO!!!')
                            ...
                            &gt;&gt;&gt; def greet(name):  # function with 1 parameter
                            ...     """Print a greeting to name."""
                            ...     greeting = 'Hello'
                            ...     print(greeting, name)
                            ...
                            &gt;&gt;&gt; def greeter(name='Stranger'):  # funtion with optional parameter
                            ...     """Print a greeting to name if given, otherwise greet Stranger."""
                            ...     greeting = 'Hi'
                            ...     print(greeting, name)
                            ...
                            ```
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ### Calling the functions

                            ```python
                            &gt;&gt;&gt; shout()
                            HELLO!!!
                            &gt;&gt;&gt; greet('Python')
                            Hello Python
                            &gt;&gt;&gt; greeter()
                            Hello Stranger
                            &gt;&gt;&gt; greeter('Monty')
                            Hello Monty
                            &gt;&gt;&gt; help(greet)
                            Help on function greet in module __main__:

                            greet(name)
                                Print a greeting to name.
                            ```
                        </textarea>
                    </section>
                </section>

                <section>
                    <h2>Named Arguments</h2>
                    <section data-markdown>
                        <textarea data-template>
                            So far, we've been passing arguments relative to their position in the paramter definition.

                            ```python
                            &gt;&gt;&gt; def print_full_name(first_name, middle_name, last_name):
                            ...     """Print the full name based on first_name, middle_name and last_name"""
                            ...     print(first_name, middle_name, last_name)
                            ...
                            &gt;&gt;&gt; print_full_name('Guido', 'van', 'Rossum')
                            Guido van Rossum
                            ```
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            Another way to pass arguments is via _named parameters_. You can use the parameter name to specify its respective argument.

                            ```python
                            &gt;&gt;&gt; print_full_name(first_name='Guido', middle_name='van', last_name='Rossum')
                            Guido van Rossum
                            &gt;&gt;&gt; print_full_name(last_name='Rossum', first_name='Guido', middle_name='van')
                            Guido van Rossum
                            ```
                        </textarea>
                    </section>
                </section>

                <section>
                    <h2>First Class Objects</h2>
                    <section>
                        <p>A <strong>first class object</strong> is an entity that can be dynamically created, destroyed, passed to a function, returned as a value, and have all the rights as other variables in the programming language have.</p>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ### `return`

                            ```python
                            &gt;&gt;&gt; greet_response = greet('Python')  # using the greet funtion from previous section
                            'Hello Python'
                            &gt;&gt;&gt; print(greet_response)
                            None
                            &gt;&gt;&gt; def new_greet(name)
                            ...     """Return a greeting to name."""
                            ...     greeting = 'Hello'
                            ...     return greeting + name
                            &gt;&gt;&gt; greet_response = new_greet('Python')
                            &gt;&gt;&gt; print(greet_response)
                            Hello Python
                            &gt;&gt;&gt; print(new_greet('Python'))  # passing the return value of a function to another function
                            Hello Python
                            ```
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            Functions can also be passed as an argument to a function.

                            ```python
                            &gt;&gt;&gt; def greeter(name):
                            ...     print('Hello', name)
                            ...
                            &gt;&gt;&gt; def super_greet(func, name):
                            ...     func(name)
                            ...
                            &gt;&gt;&gt; super_greet(greeter, 'Python')
                            Hello Python
                            ```
                        </textarea>
                    </section>
                </section>

                <section>
                    <h2>Variable Scope</h2>
                    <section data-markdown>
                        <textarea data-template>
                            Variables are scoped based on where they are declared. So far, we've been using the Python REPL which defaults to the _global_.

                            Variables declared in a function are said to be in _local_ scope. Function parameters are also local.
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ```python
                            &gt;&gt;&gt; name = 'Monty'  # global scope
                            &gt;&gt;&gt; def greet(name):  # local scope
                            ...     name = name  # also in local scope
                            ...     print(name)
                            ...
                            &gt;&gt;&gt; print(name)
                            Monty
                            &gt;&gt;&gt; greet('Guido')
                            Guido
                            &gt;&gt;&gt; print(name)
                            Monty
                            ```
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            Functions can access variables in the scope where the function was declared in; but that scope cannot access variables declared in the function.

                            ```python
                            &gt;&gt;&gt; message = 'Congratulations!'
                            &gt;&gt;&gt; def greet(name):
                            ...     print('Hello', name + '.', message)  # message is accessible
                            ...
                            &gt;&gt;&gt; greet('Guido')
                            Hello Guido. Congratulations!
                            &gt;&gt;&gt; print(name)
                            Traceback (most recent call last):
                              File "&lt;stdin&gt;", line 1, in &lt;module&gt;
                                print(name)
                            NameError: name 'name' is not defined
                            ```
                        </textarea>
                    </section>
                </section>

                <section>
                    <h2>Why Functions?</h2>
                    <section>
                        <p>Functions gives you an opportunity to name a group of statements, which makes your program easier to read and debug.</p>
                    </section>
                    <section>
                        <p>Functions can make your program smaller by eliminating repetitive code. Later, if you make a change, you only have to make it in one place.</p>
                    </section>
                    <section>
                        <p>Dividing a long program into functions allows you to debug the parts one at a time and then assemble them into a working whole.</p>
                    </section>
                    <section>
                        <p>Well-designed functions are often userful for many programs. Once you write and debug one, you can reuse it.</p>
                    </section>
                </section>

                <section>
                    <h2><a href="07-modules-and-packages.html">Next: Modules and Packages</a></h2>
                </section>

            </div>
        </div>

        <script src="lib/js/head.min.js"></script>
        <script src="js/reveal.js"></script>

        <script>
            // More info about config & dependencies:
            // - https://github.com/hakimel/reveal.js#configuration
            // - https://github.com/hakimel/reveal.js#dependencies
            Reveal.initialize({
                dependencies: [
                    { src: 'plugin/markdown/marked.js' },
                    { src: 'plugin/markdown/markdown.js' },
                    { src: 'plugin/notes/notes.js', async: true },
                    { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
                ]
            });
        </script>
    </body>
</html>
