<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <title>Classes and Objects</title>

        <link rel="stylesheet" href="css/reveal.css">
        <link rel="stylesheet" href="css/theme/solarized.css">

        <!-- Theme used for syntax highlighting of code -->
        <link rel="stylesheet" href="lib/css/zenburn.css">

        <!-- Printing and PDF exports -->
        <script>
            var link = document.createElement( 'link' );
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
            document.getElementsByTagName( 'head' )[0].appendChild( link );
        </script>
    </head>
    <body>
        <div class="reveal">
            <div class="slides">

                <section><h1>Classes and Objects</h1></section>

                <section>
                    <h2>Object Oriented Programming</h2>
                    <section>
                        <p>It's hard to talk about Classes and Objects without talking about Object Oriented Programming or OOP.</p>
                        <p>OOP is a programming <em>paradigm</em> based on the concepts of "objects", which are complex data structures that contain <em>attributes</em> and <em>methods</em>.</p>
                    </section>
                    <section>
                        <p>Objects can sometimes correspond to things found in the world.</p>
                        <ul>
                            <li>A graphics program may have objects such as "circle", "square", or "menu".</li>
                            <li>An online shopping system might have objects such as "shopping cart", "customer" and "product".</li>
                        </ul>
                    </section>
                    <section>
                        <p>Sometimes objects represent more abstract entities like</p>
                        <ul>
                            <li>an object that represents an open file</li>
                            <li>an object which provides the service of translating measurements from U.S. customary to metric</li>
                        </ul>
                    </section>
                    <section>
                        <p>As a simplification, I would say that OOP gives us a way to group together related data and the methods (or procedures) that can manipulate those data.</p>
                        <p>The data and methods, contained in an object, ensures that only the object's methods can work on the object's data &mdash; or that everything is <em>encapsulated</em>.</p>
                    </section>
                </section>

                <section>
                    <h2>Classes</h2>
                    <section>
                        <p>A class is a user-defined prototype for an object that defines a set of attributes that characterize any object of the class.</p>
                        <p class="fragment">Put simply, <strong>classes are how you implement and apply the concept of objects</strong>.</p>
                        <p class="fragment">Think of classes as "blueprints" for your objects.</p>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ### `class` Keyword

                            Classes in Python are defined by using the `class` keyword.

                            ```python
                            class MyClass:
                                """A simple example class"""
                                i = 12345

                                def f(self):
                                    return 'Hello world!'
                            ```
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            Class definitions, like function definitions (`def` statements) must be executed before they have any effect.

                            Class _instantiation_ uses function notation. For example (assuming the above class):

                            ```python
                            x = MyClass()
                            ```

                            creates a new _instance_ of the class and assigns this object to the local variable `x`.
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ```python
                            class ShoppingCart:
                                """A rudimentary class representation of a shopping cart."""
                                total_price = 0.00  # attribute; float

                                def add_item(self, item_price):  # method; item_price is a float
                                    self.total_price += item_price

                                def get_total_price(self):
                                    return self.total_price
                            ```
                        </textarea>
                    </section>
                </section>

                <section>
                    <h2>Objects</h2>

                    <section data-markdown>
                        <textarea data-template>
                            From the previous class, `ShoppingCart`, we can create an object by instantiating the class.

                            ```python
                            my_cart = ShoppingCart()  # instantiation

                            # check the initial total_amount
                            print(my_cart.get_total_price())  # should be 0.00

                            # we can now add "items" to the cart
                            my_cart.add_item(20.00)
                            # and check
                            print(my_cart.get_total_price())  # should now be 20.00
                            ```
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            The `my_cart.add_item(20.00)` code could be read as calling the method `add_item()` on the object `my_cart`.

                            Objects almost always have methods. I mention this because if you haven't noticed, __everything in Python is an object__. Strings are objects, lists are objects, dictionaries are objects.

                            That means these objects almost always have methods!
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ### `str` Methods

                            ```python
                            # Sample str methods
                            &gt;&gt;&gt; 'HELLO'.lower()
                            'hello'
                            &gt;&gt;&gt; 'hello'.upper()
                            'HELLO'
                            &gt;&gt;&gt; 'hello'.capitalize()
                            'Hello'
                            &gt;&gt;&gt; 'one,two,three'.split(',')  # returns a list
                            ['one', 'two', 'three']
                            ```
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ### `str.format()`

                            Provides the ability to do complex variable substitutions and value formatting.

                            ```python
                            # Printing out strings with values dependent on variables
                            &gt;&gt;&gt; greeting = 'Hello'
                            &gt;&gt;&gt; name = 'Python'
                            &gt;&gt;&gt; print(greeting + ' ' + name)
                            Hello Python

                            # Using .format
                            &gt;&gt;&gt; print('{} {}'.format(greeting, name))
                            Hello Python
                            &gt;&gt;&gt; print('{a} {b}'.format(a=name, b=greeting))
                            Python Hello
                            ```
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ### `list` Methods

                            ```python
                            # Sample list methods
                            &gt;&gt;&gt; li = []
                            &gt;&gt;&gt; li.append('one')
                            &gt;&gt;&gt; li
                            ['one']
                            &gt;&gt;&gt; li.append('two')
                            &gt;&gt;&gt; li
                            ['one', 'two']
                            &gt;&gt;&gt; li.pop()
                            'two'
                            &gt;&gt;&gt; li
                            ['one']
                            ```
                        </textarea>
                    </section>

                    <section data-markdown>
                        <textarea data-template>
                            ### `dict` Methods

                            ```python
                            # Sample dict methods
                            &gt;&gt;&gt; d = {'breakfast': 'spam', 'lunch': 'eggs', 'dinner': 'bacon'}
                            &gt;&gt;&gt; d.keys()
                            dict_keys(['breakfast', 'lunch', 'dinner'])
                            &gt;&gt;&gt; d.items()
                            dict_items([('breakfast', 'spam'), ('lunch', 'eggs'), ('dinner', 'bacon')])
                            &gt;&gt;&gt; d.update({'snack': 'fries'})
                            &gt;&gt;&gt; d
                            {'breakfast': 'spam', 'lunch': 'eggs', 'dinner': 'bacon', 'snack': 'fries'}
                            ```
                        </textarea>
                    </section>
                </section>

                <section>
                    <h2><a href="10-where-to-go-from-here.html">Next: Where To Go From Here</a></h2>
                </section>

            </div>
        </div>

        <script src="lib/js/head.min.js"></script>
        <script src="js/reveal.js"></script>

        <script>
            // More info about config & dependencies:
            // - https://github.com/hakimel/reveal.js#configuration
            // - https://github.com/hakimel/reveal.js#dependencies
            Reveal.initialize({
                dependencies: [
                    { src: 'plugin/markdown/marked.js' },
                    { src: 'plugin/markdown/markdown.js' },
                    { src: 'plugin/notes/notes.js', async: true },
                    { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
                ]
            });
        </script>
    </body>
</html>
