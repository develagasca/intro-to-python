#!/usr/bin/python -tt

def consonants(string):
    #TODO: Add your code here
    return

def vowels(string):
    #TODO: Add your code here
    return

def test(got, expected):
    if got == expected:
        prefix = ' OK '
    else:
        prefix = '  X '
    print('{} got: {} expected: {}'.format(prefix, repr(got), repr(expected)))


def main():
    test(consonants('abcdef'), 'Consonants: 4')
    test(consonants('aeiou'), 'Consonants: 0')
    test(vowels('abcdef'), 'Vowels: 2')
    test(vowels('bcd'), 'Vowels: 0')

if __name__ == '__main__':
    main()
