#!/usr/bin/python -tt

import argparse
import os

# import requests   # download this through pip

API_KEY = 'ab630e952b9744318273b71f2f22fc94'

def parse_api(outputfile):
    # url = 'https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=' + API_KEY
    # response = requests.get(url)
    #TODO: Add your code here
    pass

def main():
    parser = argparse.ArgumentParser(description='File parser')
    parser.add_argument('filename', help='Name of the file')
    args = parser.parse_args()

    cwd = os.getcwd()

    outputfile = os.path.join(cwd, args.filename)

    if not os.path.isfile(outputfile):
        outputfile = args.filename
    parse_api(outputfile)


if __name__ == '__main__':
    main()
