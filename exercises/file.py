#!/usr/bin/python -tt

import argparse
import os

def parse_and_count(fullfile):
    #TODO: Add your code here
    pass

def main():
    parser = argparse.ArgumentParser(description='File parser')
    parser.add_argument('filename', help='Name of the file')
    args = parser.parse_args()

    cwd = os.getcwd()

    fullfile = os.path.join(cwd, args.filename)

    if not os.path.isfile(fullfile):
        fullfile = args.filename
    parse_and_count(fullfile)


if __name__ == '__main__':
    main()
