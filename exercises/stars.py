#!/usr/bin/python -tt

def stars(count):
    #TODO: Add your code here
    return



def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print('{} got: {} expected: {}'.format(prefix, repr(got), repr(expected)))


def main():
    test(stars(-1), 'Number of stars: None')
    test(stars(0), 'Number of stars: None')
    test(stars(4), 'Number of stars: 4')
    test(stars(50), 'Number of stars: 50')
    test(stars(51), 'Number of stars: Hard to count')
    test(stars(99), 'Number of stars: Hard to count')
    test(stars(100), 'Number of stars: Too many')
    test(stars(500), 'Number of stars: Too many')

if __name__ == '__main__':
  main()
