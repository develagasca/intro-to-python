class ShoppingCart:
    """A rudimentary class representation of a shopping cart."""
    total_price = 0.00

    def add_item(self, item_price):
        self.total_price += item_price

    def get_total_price(self):
        return self.total_price

my_cart = ShoppingCart()

# check the initial total_amount
print(my_cart.get_total_price())

# add "item"
my_cart.add_item(20.00)
# and check the balance
print(my_cart.get_total_price())

print(type(my_cart))
